console.log('Loaded');

document.addEventListener("DOMContentLoaded", function() {
    var btn = document.querySelector('[type="submit"]');
    if (btn) {
        btn.click();
    }
});

var noalert = document.createElement('script');
noalert.type = "text/javascript";
noalert.textContent = "window.alert = function(x) {console.log(x)}; window.close = function() {};";
document.documentElement.appendChild(noalert);